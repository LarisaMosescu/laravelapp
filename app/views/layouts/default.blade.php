<!doctype html>
<html>
<head>
    <!-- my head section goes here -->
    <!-- my css and js goes here -->
    {{ HTML::style('css/style.css'); }}
</head>
<body>
<div class="container">
    <div class="sidebar"> @include('layouts.sidebar') </div>
    <div class="contents"> @yield('content') </div>
    <footer  class="bottom-bar"> @include('layouts.footer') </footer>
</div>
</body>
</html>
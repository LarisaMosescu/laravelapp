<?php
class LoginController extends BaseController
{

    public function test()
    {

        $results = DB::select('select * from users where user_id = ?', array(1));
        return $results;

    }

    public function doLogin()
    {

        // Getting all post data
        $data = Input::all();

        // validate the info, create rules for the inputs
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make($data, $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)// send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => Input::get('email'),
                'password' => Input::get('password')
            );
            var_dump($userdata);

        }

        $results = DB::select('select * from users where email = ? and password = ?', array($userdata['email'], $userdata['password']));
      
        // attempt to do the login
        if ($results) {

            // validation successful!
            return Redirect::intended('/');

        } else {

            // validation not successful, send back to form
            Session::flash('error', 'Something went wrong');
            return Redirect::to('login');
        }
    }

    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }

}